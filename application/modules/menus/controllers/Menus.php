<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends CI_Controller {

	/**
	 * Author  : Valdi Adrian.
	 * Created : 12 April 2019
	 */
	public function index()
	{
		$data = '';
		$layout['js']   = $this->load->view('menus/js', '', true);
		$layout['body'] = $this->load->view('menus/index', $data, true);


		$this->load->view('layouts', $layout);
	}

	function form_entry()
	{
		$this->load->view('menus/form_entry', '');
	}

	function entry_menu()
	{
		// declare varible
		$nama_menu = $this->input->post('nama_menu');
		$path = $this->input->post('path');
		$icon = $this->input->post('icon');
		$order_menu = $this->input->post('order_menu');
		$id_parent = $this->input->post('id_parent');
	}
}
