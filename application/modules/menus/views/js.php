<script>
	$('#tbl_menu').DataTable({
        responsive: true
    });

    $('#simpanMenu').click(function(){
    	$.ajax({
    		type: 'POST',
    		url: '<?php echo site_url('menus/entry_menu'); ?>',
    		data: $('#entry_menu').serialize(),
    		success: function(data)
    		{
    			$('#modalMenuEntry').modal('hide');
    		}
    	});
    })
</script>