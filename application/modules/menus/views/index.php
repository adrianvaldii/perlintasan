<div class="float-right">
	<button type="button" class="btn btn-primary btn-outline btn-raised" id="addMenu" data-toggle="modal" data-target="#modalMenuEntry"><i class="icmn-pencil7"></i> Tambah</button>
</div>
<br>
<br>
<table class="table table-bordered table-striped nowrap" width="100%" id="tbl_menu">
	<thead>
		<tr>
			<th>Nama Menu</th>
			<th>Path</th>
			<th>Parent</th>
			<th>Icon</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php
			for ($i=0; $i < 20; $i++) { 
				?>
					<tr>
						<td>Master</td>
						<td>master/index</td>
						<td></td>
						<td>icmn-archive</td>
						<td class="text-center">
							<a class="btn btn-warning btn-outline btn-raised" href="#"><i class="icmn-pencil2"></i> Ubah</a>
							<a class="btn btn-danger btn-outline btn-raised" href="#"><i class="icmn-bin"> Hapus</i></a>
						</td>
					</tr>
				<?php
			}
		?>
	</tbody>
</table>

<!-- :::::::::::::::::::::::::::::::::::::::::::::: modal ::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
<!-- Modal -->
<div class="modal fade" id="modalMenuEntry" tabindex="-1" role="dialog" aria-labelledby="EntryTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        	<form id="entry_menu">
	            <div class="modal-header">
	                <h5 class="modal-title" id="EntryTitle">Buat Menu</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	          		<div class="form-group row">
                        <div class="col-md-3">
                            <label class="form-control-label" for="l0">Nama Menu</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="nama_menu" id="l0">
                        </div>
                    </div>
                    <div class="form-group row">
                    	<div class="col-md-3">
                    		<label class="form-control-label">Lokasi Aplikasi</label>
                    	</div>
                    	<div class="col-md-9">
                    		<input type="text" class="form-control" name="path">
                    	</div>
                    </div>
                    <div class="form-group row">
                    	<div class="col-md-3">
                    		<label class="form-control-label">Icon</label>
                    	</div>
                    	<div class="col-md-9">
                    		<input type="text" class="form-control" name="icon">
                    	</div>
                    </div>
                    <div class="form-group row">
                    	<div class="col-md-3">
                    		<label class="form-control-label">Urutan Menu</label>
                    	</div>
                    	<div class="col-md-9">
                    		<input type="number" class="form-control" name="order_menu">
                    	</div>
                    </div>
                    <div class="form-group row">
                    	<div class="col-md-3">
                    		<label class="form-control-label">Parents</label>
                    	</div>
                    	<div class="col-md-9">
                    		<select class="form-control" name="id_parent">
                    			<option value="">-- SELECT PARENT --</option>
                    		</select>
                    	</div>
                    </div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	                <button type="button" class="btn btn-primary" id="simpanMenu">Simpan</button>
	            </div>
            </form>
        </div>
    </div>
</div>