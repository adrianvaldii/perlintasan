<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
		parent::__construct();

		// test
	}

	public function index()
	{
		$data = '';
		$layout['body'] = $this->load->view('dashboard/index', $data, true);


		$this->load->view('layouts', $layout);
	}
}
